new Vue({
    el: '#profile',
    data: {
        user: Object,
    },
    filters: {
        reverse: function (array) {
            return array.slice().reverse()
        }
    },
    created() {
        let user = JSON.parse(localStorage.getItem('vantrong291_github_user'));
        if (user != null) {
            this.user = user;

            let now = Date.now();
            let date = new Date(now);
            let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
            firebase.database().ref('logs/' + now).set({
                user: user.email,
                time: time,
                page: "profile"
            });

        } else location.href = '../../login';
    },
    methods: {
        logout: function () {
            localStorage.removeItem('vantrong291_github_user');
            location.href = '../../login';
        },
        obj_reverse: (obj) => {
            new_obj = {};
            rev_obj = Object.keys(obj).reverse();
            rev_obj.forEach(function (i) {
                new_obj[i] = obj[i];
            });
            return new_obj;
        }

    }
});