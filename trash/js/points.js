new Vue({
    el: '#points',
    data: {
        user: Object,
        points: Object
    },
    filters: {
        reverse: function (array) {
            return array.slice().reverse()
        }
    },
    mounted() {
        let self = this;
        $.ajax({
            url: "https://kyuu-1abfd.firebaseio.com/db/points.json",
            success: (res) => {
                self.points = res;
                let now = Date.now();
                let date = new Date(now);
                let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                // firebase.database().ref('db/points/' + now).set({
                //     date: "23 Nov 2011",
                //     title: "THE ZERO POINT",
                //     subtitle: "LET'S START THE JOURNEY",
                //     content: "Sau những đắn đo, thậm chí là tranh luận về việc yêu xa và những khó khăn của nó, chúng tôi đã đi đến quyết định cùng dắt tay nhau trong chuyến hành trình chắc chắn sẽ không dễ dàng này và cả những hành trình sau đó."
                // });
            },
        });
    },
    created() {
        let user = JSON.parse(localStorage.getItem('vantrong291_github_user'));
        if (user != null) {
            this.user = user;

            let now = Date.now();
            let date = new Date(now);
            let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
            firebase.database().ref('logs/' + now).set({
                user: user.email,
                time: time,
                page: "points"
            });

        } else location.href = '../login';
    },
    methods: {
        logout: function () {
            localStorage.removeItem('vantrong291_github_user');
            location.href = '../login';
        },
        obj_reverse: (obj) => {
            new_obj = {};
            rev_obj = Object.keys(obj).reverse();
            rev_obj.forEach(function (i) {
                new_obj[i] = obj[i];
            });
            return new_obj;
        }

    }
});