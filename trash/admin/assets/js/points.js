new Vue({
    el: '#points',
    data: {
        user: Object,
        points: Object,
        new_point: {
            title: '',
            date: '',
            subtitle: '',
            content: '',
            post_link: ''
        },
        edit_point: {
            key: '',
            title: '',
            date: '',
            subtitle: '',
            content: '',
            post_link: ''
        },
    },
    mounted() {
        let self = this;
        $.ajax({
            url: "https://kyuu-1abfd.firebaseio.com/db/points.json",
            success: (res) => {
                self.points = res;
                let now = Date.now();
                let date = new Date(now);
                let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                // firebase.database().ref('db/points/' + now).set({
                //     date: "23 Nov 2011",
                //     title: "THE ZERO POINT",
                //     subtitle: "LET'S START THE JOURNEY",
                //     content: "Sau những đắn đo, thậm chí là tranh luận về việc yêu xa và những khó khăn của nó, chúng tôi đã đi đến quyết định cùng dắt tay nhau trong chuyến hành trình chắc chắn sẽ không dễ dàng này và cả những hành trình sau đó."
                // });
            },
        });

    },
    beforeCreate() {
        let user = JSON.parse(localStorage.getItem('vantrong291_github_user'));
        if (user != null) {
            console.log(user.email);
            let userName = user.email.replace('@gmail.com','');
            if(userName == 'we291' || userName == 'vantrong291') {

            }
            else {
                location.href = '../../../login'
            }
        } else location.href = '../../../login';
    },
    created() {
        let user = JSON.parse(localStorage.getItem('vantrong291_github_user'));
        if (user != null) {
            console.log(user.email);
            this.user = user;
            setTimeout(function () {
                // $('#point-table').DataTable();

            }, 1000);
        } else location.href = '../../../login';
    },
    methods: {
        submitNewPoint: function (e) {
            let now = Date.now();
            let date = new Date(now);
            let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
            firebase.database().ref('db/points/' + now).set({
                date: this.new_point.date,
                title: this.new_point.title,
                subtitle: this.new_point.subtitle,
                content: this.new_point.content,
                post_link: this.new_point.post_link,
                updatedAt: time
            });
        },
        closeModal: function(e) {
            let self = this;
            $.ajax({
                url: "https://kyuu-1abfd.firebaseio.com/db/points.json",
                success: (res) => {
                    self.points = res;
                },
            });
        },
        edit: function(e) {
            let activeEl = e.target.ownerDocument.activeElement;
            this.edit_point.key = activeEl.getAttribute('index');
            this.edit_point.title = activeEl.getAttribute('title');
            this.edit_point.subtitle = activeEl.getAttribute('subtitle');
            this.edit_point.date = activeEl.getAttribute('date');
            this.edit_point.content = activeEl.getAttribute('content');
            this.edit_point.post_link = activeEl.getAttribute('post_link');
        },
        savePoint: function(e) {
            let key = this.edit_point.key;
            let now = Date.now();
            let date = new Date(now);
            let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
            firebase.database().ref('db/points/' + key).set({
                date: this.edit_point.date,
                title: this.edit_point.title,
                subtitle: this.edit_point.subtitle,
                content: this.edit_point.content,
                post_link: this.edit_point.post_link,
                updatedAt: time
            });

        },
        logout: function () {
            localStorage.removeItem('vantrong291_github_user');
            location.href = '../../../login';
        },

    }
});