new Vue({
    el: '#dashboard',
    data: {
        user: Object,
        points: Object,
        new_point: {
            title: '',
            date: '',
            subtitle: '',
            content: ''
        },
        edit_point: {
            key: '',
            title: '',
            date: '',
            subtitle: '',
            content: ''
        },
    },
    mounted() {
        let self = this;
        $.ajax({
            url: "https://kyuu-1abfd.firebaseio.com/db/points.json",
            success: (res) => {
                self.points = res;
                let now = Date.now();
                let date = new Date(now);
                let time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                // firebase.database().ref('db/points/' + now).set({
                //     date: "23 Nov 2011",
                //     title: "THE ZERO POINT",
                //     subtitle: "LET'S START THE JOURNEY",
                //     content: "Sau những đắn đo, thậm chí là tranh luận về việc yêu xa và những khó khăn của nó, chúng tôi đã đi đến quyết định cùng dắt tay nhau trong chuyến hành trình chắc chắn sẽ không dễ dàng này và cả những hành trình sau đó."
                // });
            },
        });

    },
    beforeCreate() {
        let user = JSON.parse(localStorage.getItem('vantrong291_github_user'));
        if (user != null) {
            console.log(user.email);
            let userName = user.email.replace('@gmail.com','');
            if(userName == 'we291' || userName == 'vantrong291') {

            }
            else {
                location.href = '../../../login'
            }
        } else location.href = '../../../login';
    },
    created() {
        let user = JSON.parse(localStorage.getItem('vantrong291_github_user'));
        if (user != null) {
            console.log(user.email);
            this.user = user;
            setTimeout(function () {
                // $('#point-table').DataTable();

            }, 1000);
        } else location.href = '../../../login';
    },
    methods: {
        logout: function () {
            localStorage.removeItem('vantrong291_github_user');
            location.href = '../../../login';
        },

    }
});